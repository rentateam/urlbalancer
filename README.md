Url Balancer
=========

Url balancer is a lightweight library for domain dynamic allocation.

  - Silex support
  - Extendable strategies
  - Extendable optional caching

Usage
--------------

```php

$urlBalancer = new \Astartsky\UrlBalancer\UrlBalancer();
$urlBalancer->setStrategy(new \Astartsky\UrlBalancer\Strategy\HashStrategy());
$urlBalancer->addBucket(new \Astartsky\UrlBalancer\Domain("static1.myawesomesite.com"));
$urlBalancer->addBucket(new \Astartsky\UrlBalancer\Domain("static2.myawesomesite.com"));
$urlBalancer->addBucket(new \Astartsky\UrlBalancer\Domain("static3.myawesomesite.com"));

$url = $urlBalancer->getUrl("/images/my_impressive_content.png");

```

License
----

MIT