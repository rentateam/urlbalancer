<?php
namespace Astartsky\UrlBalancer\Strategy;

class RandomStrategy implements StrategyInterface
{

    /**
     * @param string $url
     * @param int $buckets
     * @return int
     */
    public function choose($url, $buckets)
    {
        return mt_rand(0, $buckets - 1);
    }
}