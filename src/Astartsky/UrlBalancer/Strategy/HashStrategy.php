<?php
namespace Astartsky\UrlBalancer\Strategy;

class HashStrategy implements StrategyInterface
{

    /**
     * @param string $url
     * @param int $buckets
     * @return int
     */
    public function choose($url, $buckets)
    {
        return abs(crc32($url)) % $buckets;
    }
}
