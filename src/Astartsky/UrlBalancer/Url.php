<?php
namespace Astartsky\UrlBalancer;

class Url
{
    protected $scheme;
    protected $host;
    protected $path;
    protected $port;
    protected $pass;
    protected $user;
    protected $fragment;
    protected $query = array();

    protected $info;

    public function __construct($url)
    {
        $info = parse_url($url);

        $this->scheme = isset($info["scheme"]) ? $info["scheme"] : null;
        $this->host = isset($info["host"]) ? $info["host"] : null;
        $this->port = isset($info["port"]) ? $info["port"] : null;
        $this->user = isset($info["user"]) ? $info["user"] : null;
        $this->pass = isset($info["pass"]) ? $info["pass"] : null;
        $this->path = isset($info["path"]) ? $info["path"] : null;

        if (isset($info["query"])) {
            parse_str($info["query"], $this->query);
        } else {
            $this->query = array();
        }

        $this->fragment = isset($info["fragment"]) ? $info["fragment"] : null;
    }

    public function __toString()
    {
        $url = "";

        if ($this->scheme) {
            $url .= "{$this->scheme}://";
        }

        if ($this->user) {
            $url .= "{$this->user}";
            if ($this->pass) {
                $url .= ":{$this->pass}";
            }
            $url .= "@";
        }

        if ($this->host) {
            $url .= "{$this->host}";
        }

        if ($this->path) {
            $url .= "{$this->path}";
        }

        if ($this->query) {
            $url .= "?" . http_build_query($this->query);
        }

        if ($this->fragment) {
            $url .= "#" . $this->fragment;
        }

        return $url;
    }

    /**
     * @param string $fragment
     */
    public function setFragment($fragment)
    {
        $this->fragment = $fragment;
    }

    /**
     * @return string
     */
    public function getFragment()
    {
        return $this->fragment;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param string $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    /**
     * @return string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function setQuery($key, $value)
    {
        $this->query[$key] = $value;
    }

    /**
     * @return array
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param null $scheme
     */
    public function setScheme($scheme)
    {
        $this->scheme = $scheme;
    }

    /**
     * @return null
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * @param null $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return null
     */
    public function getUser()
    {
        return $this->user;
    }
}