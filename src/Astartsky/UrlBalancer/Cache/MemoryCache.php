<?php
namespace Astartsky\UrlBalancer\Cache;

class MemoryCache implements CacheInterface
{
    protected $array = array();

    /**
     * @param string $originUrl
     * @param string $newUrl
     */
    public function save($originUrl, $newUrl)
    {
        $this->array[$originUrl] = $newUrl;
    }

    /**
     * @param string $originUrl
     * @return int
     */
    public function get($originUrl)
    {
        return isset($this->array[$originUrl]) ? $this->array[$originUrl] : null;
    }
}