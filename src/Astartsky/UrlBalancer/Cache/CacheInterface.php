<?php
namespace Astartsky\UrlBalancer\Cache;

interface CacheInterface
{
    /**
     * @param string $originUrl
     * @param string $newUrl
     */
    public function save($originUrl, $newUrl);

    /**
     * @param string $originUrl
     * @return string
     */
    public function get($originUrl);
}